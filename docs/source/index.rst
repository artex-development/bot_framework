.. BuildABot documentation master file, created by
   sphinx-quickstart on Tue Oct 23 17:36:49 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BuildABot
=========

    print("Hello")
    >> Hello

Guide
^^^^^
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   license
   getting_started



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
